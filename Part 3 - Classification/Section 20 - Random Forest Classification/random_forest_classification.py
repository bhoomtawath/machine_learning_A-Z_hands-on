# Random Forest Classification

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Social_Network_Ads.csv')
X = dataset.iloc[:, [2, 3]].values
y = dataset.iloc[:, 4].values

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Fitting Random Forest Classification to the Training set
from sklearn.ensemble import RandomForestClassifier
# Too many estimators => overfitting
classifier = RandomForestClassifier(n_estimators = 10, criterion = 'entropy', random_state = 0)
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

def plotPrediction(x, y, set_name):
    plt.title('Logistic Regression (' + set_name + ')')
    plt.xlabel('Age')
    plt.ylabel('Estimated Salary')
    plt.legend()
    
    # BG color and splitting line
    from matplotlib.colors import ListedColormap
    X_set, y_set = x, y
    X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01),
                         np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))
    plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
                 alpha = 0.75, cmap = ListedColormap(('pink', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())    
    
    # Plotting data
    # i = 0, render red
    # i = 1, render green
    for i, k in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == k, 0], X_set[y_set == k, 1],
                    c = ListedColormap(('red', 'green'))(i))    
        # why label = j?, ListedColormap doesn't has label
    plt.show()
    
plotPrediction(X_train, y_train, 'Train')

# See that regions (background) have the same shape as the training graph
# because the regions are fitted by the training set
plotPrediction(X_test, y_test, 'Test')