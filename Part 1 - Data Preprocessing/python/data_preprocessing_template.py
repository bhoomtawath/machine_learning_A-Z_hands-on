# Data Preprocessing Template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
np.set_printoptions(precision=3)

# Importing the dataset
dataset = pd.read_csv('../Data.csv')
# [:, :-1] : is taking all the lines, :-1 is taking every columns except the last one
X = dataset.iloc[:, :-1].values
# y is the result
y = dataset.iloc[:, 3].values

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
# Already fitted, so transform only
X_test = sc_X.transform(X_test)"""

# Not need to perform feature scaling on y, because this is a classification problem
# only needed in regression problem
"""sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""