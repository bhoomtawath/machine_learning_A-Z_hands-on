# Multiple Linear Regression

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('50_Startups.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 4].values

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder = LabelEncoder()
X[:, 3] = labelencoder.fit_transform(X[:, 3])
onehotencoder = OneHotEncoder(categorical_features = [3])
X = onehotencoder.fit_transform(X).toarray()

# Avoiding the Dummy Variable Trap
# n-1 categorial variable
# Actually, sklearn.linear_model can already take care of this, so this isn't needed
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
# sklearn will also take care of this
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""

# Fitting Multiple Linear Regression to the Training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# Predicting the Test set results
y_pred = regressor.predict(X_test)

# Backward Elimination
import statsmodels.formula.api as sm
# Add constant to the equation (other library already add that, but not sm)
X = np.append(arr = np.ones((50, 1)).astype(int), values = X, axis = 1)

# 1. Select SL = 0.05

# 2. Include all variables in X_opt (Fit full model with all possible predictors step)
X_opt = X[:, [0, 1, 2, 3, 4, 5]]
# Fit the full model
regressor_ols = sm.OLS(endog = y, exog = X_opt).fit()

# 3. Find a predictor with highest P-value
regressor_ols.summary()

# 4. Remove the highest P-value (col 2)
X_opt = X[:, [0, 1, 3, 4, 5]]

# 5. Fit the model again
regressor_ols = sm.OLS(endog = y, exog = X_opt).fit()
regressor_ols.summary()
# col1 is has the highest P-value
X_opt = X[:, [0, 3, 4, 5]]

# 5. Fit the model again
regressor_ols = sm.OLS(endog = y, exog = X_opt).fit()
regressor_ols.summary()

# 5. Fit the model again
# col4 is has the highest P-value
X_opt = X[:, [0, 3, 5]]
regressor_ols = sm.OLS(endog = y, exog = X_opt).fit()
regressor_ols.summary()

# 5. Fit the model again
# col5 is has the highest P-value
X_opt = X[:, [0, 3]]
regressor_ols = sm.OLS(endog = y, exog = X_opt).fit()
# No P-value is higher than SL (0.05)
regressor_ols.summary()

SL = 0.05

# Automatic backward elimination with only P-value
def backwardElimination(x, sl):
    numVars = len(x[0])
    for i in range(0, numVars):
        regressor_ols = sm.OLS(y, x).fit()
        maxVar = max(regressor_ols.pvalues).astype(float)
        if maxVar > sl:
            # Find max's index
            for j in range(0, numVars - i):
                if (regressor_ols.pvalues[j].astype(float) == maxVar):
                    x = np.delete(x, j, 1)
    
    regressor_ols.summary()
    return x

SL = 0.05
x_opt = X [:, [0, 1, 2, 3, 4, 5]]
backwardElimination(x_opt, SL)

# Automatic backward elimination with P-value and R-squared
def backwardEliminationWithRsqrt(x, sl):
    numVars = len(x[0])
    temp = np.zeros((50, 6)).astype(int)
    for i in range(0, numVars):
        regressor_ols = sm.OLS(y, x).fit()
        maxVar = max(regressor_ols.pvalues).astype(float)
        adjR_before = regressor_ols.rsquared_adj.astype(float)
        if maxVar > sl:
            # Find max's index
            for j in range(0, numVars - i):
                if (regressor_ols.pvalues[j].astype(float) == maxVar):
                    temp[:, j] = x[:, j]
                    x = np.delete(x, j, 1)
                    tmp_regressor = sm.OLS(y, x).fit()
                    adjR_after = tmp_regressor.rsquared_adj.astype(float)
                    if (adjR_before >= adjR_after):
                        x_rollback = np.hstack((x, temp[:, [0,j]]))
                        x_rollback = np.delete(x_rollback, j, 1)
                        print(regressor_ols.summary())
                        return x_rollback
                    else:
                        continue
        
        regressor_ols.summary()
        return x

backwardEliminationWithRsqrt(x_opt, SL)